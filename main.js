var startPoint = [50, 14.5]
var map = L.map('map', {editable: true}).setView(startPoint, 6),
    tilelayer = L.tileLayer('http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {maxZoom: 20, attribution: 'Data \u00a9 <a href="http://www.openstreetmap.org/copyright"> OpenStreetMap Contributors </a> Tiles \u00a9 HOT'}).addTo(map)

    L.EditControl = L.Control.extend({

        options: {
            position: 'topleft',
            callback: null,
            kind: '',
            html: ''
        },

        onAdd: function (map) {
            var container = L.DomUtil.create('div', 'leaflet-control leaflet-bar'),
                link = L.DomUtil.create('a', '', container)

            link.href = '#'
            link.title = 'Create a new ' + this.options.kind
            link.innerHTML = this.options.html
            L.DomEvent.on(link, 'click', L.DomEvent.stop)
                      .on(link, 'click', function () {
                        window.LAYER = this.options.callback.call(map.editTools)
                      }, this)

            return container
        }

    })

    var markers = [L.marker([50, 14.8],).addTo(map), L.marker([50, 14.8],).addTo(map), L.marker([50, 14.8],).addTo(map)]


    var resizeLoc 
    var circle = L.circle([50, 14.5], {radius: 140000}).addTo(map)
    circle.enableEdit()
    circle.on('editable:editing', setPoints)
    circle.on('editable:dragstart', function() {
        resizeLoc = circle.editor._resizeLatLng
        var center = circle.getLatLng()
        azimuth = getBearing(center.lat, center.lng, resizeLoc.lat, resizeLoc.lng)
    })
    circle.on('editable:drag', function(e) {
        circle.resizeLatLng = resizeLoc
        setPoints(e)})
    circle.on('dblclick', L.DomEvent.stop).on('dblclick', circle.toggleEdit)

    setPoints({lat: startPoint[0], lng: startPoint[1]})
    
   
    function setPoints(e) {
        resizeLoc = circle.editor._resizeLatLng
        let center = circle.getLatLng()

        azimuth = getBearing(center.lat, center.lng, resizeLoc.lat, resizeLoc.lng)

        let radius = circle.getRadius() / 1000
        let angleStep = 360 / 3
        var angle = azimuth + angleStep
        for (var i = 1; i <= 3; i++) {
            
            let res = getDestinationPoint(center.lat, center.lng, radius, angle)
            let point = L.latLng(res.lat, res.lng) 
            
            markers[i-1].setLatLng(point)
            angle += angleStep
        }
    }

    function toRadians(degrees) {
        return degrees * Math.PI / 180
    }

    function toDegrees(radians) {
        return radians * 180 / Math.PI
    }

    function getDestinationPoint(lat, lng, distance, bearing) {
        let earthRadius = 6371
        let latRadians = toRadians(lat)
        var lngRadians = toRadians(lng)
        let bearingRadians = toRadians(bearing)

        let newLat = Math.asin(Math.sin(latRadians) * Math.cos(distance / earthRadius) +
                    Math.cos(latRadians) * Math.sin(distance / earthRadius) *
                    Math.cos(bearingRadians))
        let newLng = lngRadians + Math.atan2(Math.sin(bearingRadians) *
                    Math.sin(distance / earthRadius) * Math.cos(latRadians),
                    Math.cos(distance / earthRadius) - Math.sin(latRadians) *
                    Math.sin(newLat))

        newLat = toDegrees(newLat)
        newLng = toDegrees(newLng)

        return {lat: newLat, lng: newLng}
    }

    function getBearing(startLat, startLng, destLat, destLng) {
        let startLatRad = startLat * Math.PI / 180
        let startLngRad = startLng * Math.PI / 180
        let destLatRad = destLat * Math.PI / 180
        let destLngRad = destLng * Math.PI / 180
        
        let y = Math.sin(destLngRad - startLngRad) * Math.cos(destLatRad)
        let x = Math.cos(startLatRad) * Math.sin(destLatRad) - Math.sin(startLatRad) * Math.cos(destLatRad) * Math.cos(destLngRad - startLngRad)
        
        let bearingRad = Math.atan2(y, x)
        let bearingDeg = bearingRad * 180 / Math.PI
        
        return (bearingDeg + 360) % 360
    }