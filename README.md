# Jet Lag Tag Planner
Super simple panner for the [Jet Lag the Game's game of tag](https://www.youtube.com/watch?v=q2tJqO6nCSc&list=PLB7ZcpBcwdC5B-l2FQNOPJVFpqF0QVxfG). 
In the game, three players start at some location and each of the players wants to reach it's target location. This tool aims at helping at choosing the start and target location.


It uses a Leaflet js library to do all the map and drawing stuff. I'm not that into js, so dont @ me.  
 